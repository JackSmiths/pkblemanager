//
//  AppDelegate.h
//  PKBLEManagerSample
//
//  Created by mopellet on 2017/7/5.
//  Copyright © 2017年 pkwans. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

