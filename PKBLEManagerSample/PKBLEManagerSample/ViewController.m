//
//  ViewController.m
//  PKBLEManagerSample
//
//  Created by mopellet on 2017/7/5.
//  Copyright © 2017年 pkwans. All rights reserved.
//

#import "ViewController.h"
#import "PKBLEManager.h"
@interface ViewController () <PKBLEManagerDelegate>
@property (strong, nonatomic) PKBLEManager *bleManager;
@end

@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
//        在 iOS 10 之后需要在 Info.plist 文件里面设置 NSBluetoothPeripheralUsageDescription 字段，添加访问蓝牙权限的描述，否则强行访问蓝牙功能会造成 Crash。
        self.bleManager = [PKBLEManager pk_shareInstance];
        [self.bleManager pk_addBLEManagerDelegate:self];
    
}

- (void)bleDidDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"%@",peripheral.identifier.UUIDString);
}

- (IBAction)search:(UIButton *)sender {
    [self.bleManager pk_scanPeripheralsWithServices:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
