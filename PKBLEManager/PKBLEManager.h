
#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "PKBLEManagerDelegate.h"

NS_ASSUME_NONNULL_BEGIN


typedef void(^PKBLEManagerCompletionHandler)(id _Nullable data, NSError * _Nullable error);
/*! 蓝牙4.0管理类 */
@interface PKBLEManager : NSObject
/*! 扫描到的从属蓝牙设备 */
@property (strong, readonly, nonatomic) NSMutableArray<CBPeripheral *> *peripherals;
/*! 已连接的从属蓝牙设备（不要设置此类的delegate，否PKBLEManager其它方法将失效）*/
@property (strong, readonly, nonatomic, nullable) CBPeripheral *activePeripheral;
/*! 扫描已连接的从属蓝牙设备的特征，格式为 key: service UUIDString, value: service.characteristics */
@property (strong, readonly, nonatomic) NSDictionary<NSString *, NSArray<CBCharacteristic *> *> *activeServicesCharacteristics;
/*! 是否正在连接从属蓝牙设备 */
@property (assign, readonly, nonatomic) BOOL connecting;
/*! 连接中的从属蓝牙设备 */
@property (weak, readonly, nonatomic, nullable) CBPeripheral *connectingPeripheral;
/*! PKBLEManager 单例 */
+ (instancetype)pk_shareInstance;

- (void)pk_addBLEManagerDelegate:(id<PKBLEManagerDelegate>)delegate;

- (void)pk_removeBLEManagerDelegate:(id<PKBLEManagerDelegate>)delegate;

- (void)pk_setScanOptions:(nullable NSDictionary<NSString *, id> *)options;

- (void)pk_setConnectOptions:(nullable NSDictionary<NSString *, id> *)options;

- (void)pk_setAllSupportedCharacteristicNotifyUUIDStrings:(NSArray *)UUIDStrings;
/**
 *  扫描从属蓝牙设备
 *
 *  @param serviceUUIDs 指定扫描包含特定服务的从属蓝牙设备（传nil扫描所有蓝牙4.0设备）
 *
 *  @return 如果手机蓝牙未打开返回NO，其它情况YES
 */
- (BOOL)pk_scanPeripheralsWithServices:(nullable NSArray<CBUUID *> *)serviceUUIDs;

/**
 *  扫描从属蓝牙设备
 *
 *  @param serviceUUIDs 指定扫描包含特定服务的从属蓝牙设备（传nil扫描所有蓝牙4.0设备）
 *  @param timeout      扫描超时时间（超时后停止扫描）
 *
 *  @return 如果手机蓝牙未打开返回NO，其它情况YES
 */
- (BOOL)pk_scanPeripheralsWithServices:(nullable NSArray<CBUUID *> *)serviceUUIDs
                               timeout:(NSTimeInterval)timeout;

/**
 *  停止扫描从属蓝牙设备
 */
- (void)pk_stopScan;

/**
 *  连接蓝牙从属设备
 *
 *  @param peripheral 蓝牙从属设备
 *  @param completion 回调方法(蓝牙从属设备,错误信息)
 */
- (void)pk_connectPeripheral:(CBPeripheral *)peripheral
            completionHandler:(PKBLEManagerCompletionHandler)completion;


/**
 *  断开从属蓝牙设备的连接
 *
 *  @param peripheral 要断开的从属蓝牙设备。传nil的时候断开连接中的从属蓝牙设备
 */
- (void)pk_cancelPeripheralConnection:(nullable CBPeripheral *)peripheral;

/**
 *  开启/关闭监听从属蓝牙设备某个服务的特征
 *
 *  @param p     从属蓝牙设备
 *  @param on    是否监听
 *  @param sUUID 服务的UUID
 *  @param cUUID 特征的UUID
 */
- (void)pk_notifyPeripheral:(CBPeripheral *)p on:(BOOL)on forService:(CBUUID *)sUUID characteristicUUID:(CBUUID *)cUUID;

/**
 *  读取从属蓝牙设备某个服务的特征
 *
 *  @param p     从属蓝牙设备
 *  @param sUUID 服务的UUID
 *  @param cUUID 特征的UUID
 */
- (void)pk_readPeripheral:(CBPeripheral *)p valueForService:(CBUUID *)sUUID characteristicUUID:(CBUUID *)cUUID;

/**
 *  向从属蓝牙设备发送数据
 *
 *  @param p     从属蓝牙设备
 *  @param sUUID 服务的UUID
 *  @param cUUID 特征的UUID
 *  @param data  要发送的数据
 */
- (void)pk_writePeripheral:(CBPeripheral *)p forService:(CBUUID *)sUUID characteristicUUID:(CBUUID *)cUUID withData:(NSData *)data;

/**
 *  扫描从属蓝牙设备的所有特征
 *
 *  @param p 从属蓝牙设备
 */
- (void)pk_discoverAllCharacteristicsForPeripheral:(CBPeripheral *)p;

#pragma mark - Helper

/**
 *  将特征的属性转换成NSString描述
 *
 *  @param properties 特征的属性（Broadcast、Read、Notify...）
 *
 *  @return 特征的属性描述
 */
+ (NSString *)pk_characteristicPropertiesString:(CBCharacteristicProperties)properties;

@end

NS_ASSUME_NONNULL_END
