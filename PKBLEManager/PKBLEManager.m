

#import "PKBLEManager.h"

@interface PKBLEManager ()<CBCentralManagerDelegate, CBPeripheralDelegate>
/*! 蓝牙扫描、连接类（不要设置此类的delegate，否PKBLEManager其它方法将失效）*/
@property (strong, readonly, nonatomic) CBCentralManager *centraManager;
//@property (weak, nonatomic) id<PKBLEManagerDelegate> delegate;
@property (strong, nonatomic) NSDictionary<NSString *, NSArray<CBCharacteristic *> *> *activeServicesCharacteristics;

@property (strong, nonatomic) NSMutableSet *delegates;
@property (strong, nonatomic) NSDictionary *scanOptions;
@property (strong, nonatomic) NSDictionary *connectOptions;
@property (strong, nonatomic) NSArray *allSupportedCharacteristicNotifyUUIDStrings;

@property (copy, nonatomic) PKBLEManagerCompletionHandler connectCompletionHandler;
@end

@implementation PKBLEManager


#pragma mark - Life Cycle

static id _instance;

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (_instance == nil) {
            _instance = [super allocWithZone:zone];
        }
    });
    return _instance;
}

+ (instancetype)pk_shareInstance {
    return [[[self class] alloc] init];
}

- (id)copyWithZone:(NSZone *)zone {
    return _instance;
}

- (id)mutableCopyWithZone:(NSZone *)zone {
    return _instance;
}

- (void)dealloc {
    [self pk_cancelPeripheralConnection:_activePeripheral];
}

- (instancetype)init {
    if (self = [super init]) {
        _centraManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        _delegates = [NSMutableSet set];
        _peripherals = [NSMutableArray array];
        _scanOptions = @{CBCentralManagerScanOptionAllowDuplicatesKey:[NSNumber numberWithBool:NO]};
        _connectOptions = @{CBConnectPeripheralOptionNotifyOnDisconnectionKey:@YES};
    }
    return self;
}

#pragma mark - Public

- (void)pk_addBLEManagerDelegate:(id<PKBLEManagerDelegate>)delegate {
    if ([delegate conformsToProtocol:@protocol(PKBLEManagerDelegate)]) {
        [self.delegates addObject:delegate];
    }
}

- (void)pk_removeBLEManagerDelegate:(id<PKBLEManagerDelegate>)delegate {
    if ([delegate conformsToProtocol:@protocol(PKBLEManagerDelegate)] &&
        [self.delegates containsObject:delegate]) {
        [self.delegates removeObject:delegate];
    }
}

- (void)pk_setScanOptions:(nullable NSDictionary<NSString *, id> *)options {
    _scanOptions = options;
}

- (void)pk_setConnectOptions:(nullable NSDictionary<NSString *, id> *)options {
    _connectOptions = options;
}

- (void)pk_setAllSupportedCharacteristicNotifyUUIDStrings:(NSArray *)UUIDStrings {
    _allSupportedCharacteristicNotifyUUIDStrings = UUIDStrings;
}

- (BOOL)pk_scanPeripheralsWithServices:(nullable NSArray<CBUUID *> *)serviceUUIDs {
    if (_centraManager.state != CBCentralManagerStatePoweredOn) {
        return NO;
    }
    
    if (_peripherals.count) {
        [_peripherals removeAllObjects];
    }
    // CBCentralManagerScanOptionSolicitedServiceUUIDsKey  后台的时候
    // ios支持在APP进入后台后的蓝牙通讯，但是对于扫描设备，在进入后台后，为了进行省电，则做了处理
    // 当进入后台后，只能从原先的多重发现变为单一设备发现，并且要指定该设备的UUID及service UUID。
    
    if (_connecting) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_centraManager scanForPeripheralsWithServices:serviceUUIDs options:_scanOptions];
        });
        
        return YES;
    }
    
    [_centraManager scanForPeripheralsWithServices:serviceUUIDs options:_scanOptions];
    
    return YES;
}

- (BOOL)pk_scanPeripheralsWithServices:(nullable NSArray<CBUUID *> *)serviceUUIDs
                               timeout:(NSTimeInterval)timeout {
    if (_centraManager.state != CBCentralManagerStatePoweredOn) {
        return NO;
    }
    
    if (_peripherals.count) {
        [_peripherals removeAllObjects];
    }
    
    
    if (_connecting) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [NSTimer scheduledTimerWithTimeInterval:(float)timeout target:self selector:@selector(pk_scanTimeout:) userInfo:nil repeats:NO];
            [_centraManager scanForPeripheralsWithServices:serviceUUIDs options:_scanOptions];
        });
    } else {
        [NSTimer scheduledTimerWithTimeInterval:(float)timeout target:self selector:@selector(pk_scanTimeout:) userInfo:nil repeats:NO];
        [_centraManager scanForPeripheralsWithServices:serviceUUIDs options:_scanOptions];
    }
    return YES;
}


- (void)pk_stopScan {
    [_centraManager stopScan];
}

- (void)pk_connectPeripheral:(CBPeripheral *)peripheral
           completionHandler:(PKBLEManagerCompletionHandler)completion{
    if (_activePeripheral.state == CBPeripheralStateConnected
        && [_activePeripheral.identifier.UUIDString isEqualToString:peripheral.identifier.UUIDString]) {
        completion(_activePeripheral, nil);
        return;
    }
    
    if (_activePeripheral) {
        [self pk_cancelPeripheralConnection:_activePeripheral];
    }
//    NSDictionary *options = @{
//                              CBConnectPeripheralOptionNotifyOnDisconnectionKey:@YES,
//                              CBConnectPeripheralOptionNotifyOnConnectionKey:@YES,
//                              CBConnectPeripheralOptionNotifyOnNotificationKey:@YES
//                              };
    
    self.connectCompletionHandler = completion;
    
    [_centraManager connectPeripheral:peripheral
                              options:_connectOptions];
    _connecting = YES;
    _connectingPeripheral = peripheral;
}



- (void)pk_cancelPeripheralConnection:(CBPeripheral *)peripheral {
    if (peripheral) {
        [self disconnectPeripheral:peripheral];
    } else if (_activePeripheral) {
        [self disconnectPeripheral:_activePeripheral];
    }
}

- (void)pk_notifyPeripheral:(CBPeripheral *)p on:(BOOL)on forService:(CBUUID *)sUUID characteristicUUID:(CBUUID *)cUUID {
    CBService *service = [self searchPeripheral:p serviceWithUUID:sUUID];
    if (!service) {
        return;
    }
    
    CBCharacteristic *characteristic = [self searchCharacteristicWithUUID:cUUID inService:service];
    if (!characteristic) {
        return;
    }
    
    [p setNotifyValue:on forCharacteristic:characteristic];
}

- (void)pk_readPeripheral:(CBPeripheral *)p valueForService:(CBUUID *)sUUID characteristicUUID:(CBUUID *)cUUID {
    CBService *service = [self searchPeripheral:p serviceWithUUID:sUUID];
    if (!service) {
        return;
    }
    
    CBCharacteristic *characteristic = [self searchCharacteristicWithUUID:cUUID inService:service];
    if (!characteristic) {
        return;
    }
    
    [p readValueForCharacteristic:characteristic];
}

- (void)pk_writePeripheral:(CBPeripheral *)p forService:(CBUUID *)sUUID characteristicUUID:(CBUUID *)cUUID withData:(NSData *)data
{
    CBService *service = [self searchPeripheral:p serviceWithUUID:sUUID];
    if (!service) {
        return;
    }
    
    CBCharacteristic *characteristic = [self searchCharacteristicWithUUID:cUUID inService:service];
    if (!characteristic) {
        return;
    }
    
    [p writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
}

#pragma mark - Private

- (void)pk_scanTimeout:(NSTimer *)timer
{
    [self pk_stopScan];
}

- (void)disconnectPeripheral:(CBPeripheral *)peripheral {
    if (peripheral.state == CBPeripheralStateConnected) {
        for (CBService * service in peripheral.services) {
            if (service.characteristics) {
                for (CBCharacteristic * characteristic in service.characteristics) {
                    if (characteristic.isNotifying) {
                        //取消
                        [peripheral setNotifyValue:NO forCharacteristic:characteristic];
                        break;
                    }
                }
            }
        }
        
        [_centraManager cancelPeripheralConnection:peripheral];
    }
    
    if ([_activePeripheral isEqual:peripheral]) {
        _activePeripheral = nil;
    }
    
    [_peripherals removeObject:peripheral];
}

#pragma mark - Helper

+ (NSString *)pk_characteristicPropertiesString:(CBCharacteristicProperties)properties {
    NSMutableString *propertyName = [NSMutableString string];
    if (properties & CBCharacteristicPropertyBroadcast) {
        [propertyName appendString:@"Broadcast "];
    }
    
    if (properties & CBCharacteristicPropertyRead) {
        [propertyName appendString:@"Read "];
    }
    
    if (properties & CBCharacteristicPropertyWriteWithoutResponse) {
        [propertyName appendString:@"WriteWithoutResponse "];
    }
    
    if (properties & CBCharacteristicPropertyWrite) {
        [propertyName appendString:@"Write "];
    }
    
    if (properties & CBCharacteristicPropertyNotify) {
        [propertyName appendString:@"Notify "];
    }
    
    if (properties & CBCharacteristicPropertyIndicate) {
        [propertyName appendString:@"Indicate "];
    }
    
    if (properties & CBCharacteristicPropertyAuthenticatedSignedWrites) {
        [propertyName appendString:@"AuthenticatedSignedWrites "];
    }
    
    if (properties & CBCharacteristicPropertyExtendedProperties) {
        [propertyName appendString:@"ExtendedProperties "];
    }
    
    if (properties & CBCharacteristicPropertyNotifyEncryptionRequired) {
        [propertyName appendString:@"NotifyEncryptionRequired "];
    }
    
    if (properties & CBCharacteristicPropertyIndicateEncryptionRequired) {
        [propertyName appendString:@"IndicateEncryptionRequired "];
    }
    
    return propertyName.length ? propertyName : nil;
}

- (NSString *)stringForCentralManagerState:(CBCentralManagerState)state {
    switch(state)
    {
        case CBCentralManagerStateUnknown:
            return @"State unknown (CBCentralManagerStateUnknown)";
        case CBCentralManagerStateResetting:
            return @"State resetting (CBCentralManagerStateUnknown)";
        case CBCentralManagerStateUnsupported:
            return @"State BLE unsupported (CBCentralManagerStateResetting)";
        case CBCentralManagerStateUnauthorized:
            return @"State unauthorized (CBCentralManagerStateUnauthorized)";
        case CBCentralManagerStatePoweredOff:
            return @"State BLE powered off (CBCentralManagerStatePoweredOff)";
        case CBCentralManagerStatePoweredOn:
            return @"State powered up and ready (CBCentralManagerStatePoweredOn)";
        default:
            return @"State unknown";
    }
    
    return @"Unknown state";
}

- (CBService *)searchPeripheral:(CBPeripheral *)p serviceWithUUID:(CBUUID *)UUID {
    for (CBService *service in p.services) {
        if ([service.UUID.UUIDString isEqualToString:UUID.UUIDString]) {
            return service;
        }
    }
    
    return nil; //Service not found on this peripheral
}

- (CBCharacteristic *)searchCharacteristicWithUUID:(CBUUID *)UUID inService:(CBService*)service {
    for (CBCharacteristic *characteristic in service.characteristics) {
        if ([characteristic.UUID.UUIDString isEqualToString:UUID.UUIDString]) {
            return characteristic;
        }
    }
    
    return nil; //Characteristic not found on this service
}

- (void)pk_discoverAllCharacteristicsForPeripheral:(CBPeripheral *)p {
    for (int i=0; i < p.services.count; i++) {
        CBService *s = [p.services objectAtIndex:i];
        //        printf("Fetching characteristics for service with UUID : %s\r\n",[self stringForCBUUID:s.UUID]);
        [p discoverCharacteristics:nil forService:s];
    }
}

#pragma mark - CBCentralManagerDelegate


/**
 *  手机蓝牙状态更新回调
 *
 *  @param central 手机蓝牙管理类
 */
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    if (central.state != CBCentralManagerStatePoweredOn) {
        // In a real app, you'd deal with all the states correctly
        [self pk_cancelPeripheralConnection:_activePeripheral];
        [_peripherals removeAllObjects];
    }
    
    for (id delegate in self.delegates) {
        if ([delegate respondsToSelector:@selector(bleCentralManagerDidUpdateState:)] ) {
            [delegate bleCentralManagerDidUpdateState:(CBCentralManagerState)central.state];
        }
    }
}


- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary<NSString *, id> *)dict {
    for (id delegate in self.delegates) {
        if ([delegate respondsToSelector:@selector(bleWillRestoreState:)] ) {
            [delegate bleWillRestoreState:dict];
        }
    }
}

/**
 *  扫描到从属设备（脑电蓝牙）的回调
 *
 *  @param central           手机蓝牙管理类
 *  @param peripheral        从属设备（脑电蓝牙）
 *  @param advertisementData 从属设备广播数据
 *  @param RSSI              从属设备信号大小
 */
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    for(int i = 0; i < _peripherals.count; i++) {
        CBPeripheral *p = [_peripherals objectAtIndex:i];
        
        if ((p.identifier == NULL) || (peripheral.identifier == NULL))
            continue;
        
        if ([p.identifier.UUIDString isEqualToString:peripheral.identifier.UUIDString]) {
            [_peripherals replaceObjectAtIndex:i withObject:peripheral];
            return;
        }
    }
    
    [_peripherals addObject:peripheral];
    
    peripheral.delegate = self;
    [peripheral discoverServices:nil];
    
    for (id delegate in self.delegates) {
        if ([delegate respondsToSelector:@selector(bleDidDiscoverPeripheral:advertisementData:RSSI:)] ) {
            [delegate bleDidDiscoverPeripheral:peripheral advertisementData:advertisementData RSSI:RSSI];
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    [peripheral discoverServices:nil];
    _activePeripheral = peripheral;
    _activePeripheral.delegate = self;
    _connecting = NO;
    _connectingPeripheral = nil;
    
    if ([peripheral.identifier.UUIDString isEqualToString:_activePeripheral.identifier.UUIDString]) {
        for (id delegate in self.delegates) {
            if ([delegate respondsToSelector:@selector(bleDidConnectPeripheral:)] ) {
                [delegate bleDidConnectPeripheral:peripheral];
            }
        }
    }
    if (self.connectCompletionHandler) {
        self.connectCompletionHandler(peripheral, nil);
    }
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    [self pk_cancelPeripheralConnection:peripheral];
    _connecting = NO;
    _connectingPeripheral = nil;
    
    for (id delegate in self.delegates) {
        if ([delegate respondsToSelector:@selector(bleDidFailToConnectPeripheral:error:)] ) {
            [delegate bleDidFailToConnectPeripheral:peripheral error:error];
        }
    }
    
    if (self.connectCompletionHandler) {
        self.connectCompletionHandler(nil, error);
    }
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    [self disconnectPeripheral:peripheral];
    
    for (id delegate in self.delegates) {
        if ([delegate respondsToSelector:@selector(bleDidDisconnectPeripheral:withError:)] ) {
            [delegate bleDidDisconnectPeripheral:peripheral withError:error];
        }
    }
}

#pragma mark - CBPeripheralDelegate

/**
 *  扫描从属设备服务的回调
 *
 *  @param peripheral 从属设备
 *  @param error      出错返回
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    if (!error) {
        [self pk_discoverAllCharacteristicsForPeripheral:peripheral];
        
        for (id delegate in self.delegates) {
            if ([delegate respondsToSelector:@selector(bleDidDiscoverServicesForPeripheral:)] ) {
                [delegate bleDidDiscoverServicesForPeripheral:peripheral];
            }
        }
        
        if ([peripheral.identifier.UUIDString isEqualToString:_activePeripheral.identifier.UUIDString]) {
            _activeServicesCharacteristics = nil;
            _activeServicesCharacteristics = [NSMutableDictionary dictionary];
            for (CBService *service in peripheral.services) {
                [_activeServicesCharacteristics setValue:@[] forKey:service.UUID.UUIDString];
            }
        }
    }
}

/**
 *  扫描从属设备服务中的特征
 *
 *  @param peripheral 从属设备
 *  @param service    服务
 *  @param error      出错返回
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    if (!error) {
        for (id delegate in self.delegates) {
            if ([delegate respondsToSelector:@selector(bleDidDiscoverPeripheral:characteristicsforService:)] ) {
                [delegate bleDidDiscoverPeripheral:peripheral characteristicsforService:service];
            }
        }
        
        if ([peripheral.identifier.UUIDString isEqualToString:_activePeripheral.identifier.UUIDString]) {
            [_activeServicesCharacteristics setValue:service.characteristics forKey:service.UUID.UUIDString];
        }
        
        if (_allSupportedCharacteristicNotifyUUIDStrings.count) {
            NSArray *UUIDStrings = _allSupportedCharacteristicNotifyUUIDStrings;
            for (CBCharacteristic *characteristic in service.characteristics) {
                for (NSString *UUIDString in UUIDStrings) {
                    if ([characteristic.UUID.UUIDString isEqualToString:UUIDString]) {
                        [self pk_notifyPeripheral:peripheral
                                               on:YES
                                       forService:service.UUID
                               characteristicUUID:characteristic.UUID];
                        break;
                    }
                }
            }
        }
        else {
            for (CBCharacteristic *characteristic in service.characteristics) {
                [self pk_notifyPeripheral:peripheral
                                       on:YES
                               forService:service.UUID
                       characteristicUUID:characteristic.UUID];
            }
        }
    }
}

/**
 *  从属设备的特征状态更新回调
 *
 *  @param peripheral     从属设备
 *  @param characteristic 特征
 *  @param error          出错返回
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
}

/**
 *  从属设备的特征值返回回调，在此获取特征值更新的数据
 *
 *  @param peripheral     从属设备
 *  @param characteristic 特征
 *  @param error          出错返回
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (!error) {
        for (id delegate in self.delegates) {
            if ([delegate respondsToSelector:@selector(bleDidUpdateValueForCharacteristic:)] ) {
               [delegate bleDidUpdateValueForCharacteristic:characteristic];
            }
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    for (id delegate in self.delegates) {
        if ([delegate respondsToSelector:@selector(bleDidWriteValueForCharacteristic:error:)] ) {
            [delegate bleDidWriteValueForCharacteristic:characteristic error:error];
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error  {
    for (id delegate in self.delegates) {
        if ([delegate respondsToSelector:@selector(bleDidUpdateRSSI:)] ) {
             [delegate bleDidUpdateRSSI:RSSI];
        }
    }
}

@end
